import { Response } from 'express'

export class ErrorHandler extends Error {
  readonly statusCode: number
  readonly type: string
  readonly errorOutput: string | Object

  constructor (statusCode: number, errorOutput: string | Object, type?: string) {
    super()
    this.statusCode = statusCode
    this.type = type || 'Error'
    this.errorOutput = errorOutput
  }
}

export const handleError = (err: ErrorHandler, res: Response) => {
  const { statusCode, type, errorOutput } = err
  res.status(statusCode).json({
    status: type,
    statusCode,
    errorOutput
  })
}
