import express, { Request, Response, NextFunction } from 'express'
import { Invoice } from '../models/invoice'
import { Client } from '../models/client'
import { Product } from '../models/product'
import { ValidateRequiredFields, FilterRelevantFields } from '../utils'
import { ErrorHandler } from '../errorHandler'

const router = express.Router()

const processNewClient = async (clientData: Object) => {
  if (!clientData) return
  const errorsObj = ValidateRequiredFields(['name', 'streetAddress', 'city'], clientData)

  if (Object.keys(errorsObj).length > 0) {
    throw new ErrorHandler(500, errorsObj, 'ValidationError')
  }
  const newClient = Object.assign(new Client(), clientData)
  return newClient.save()
}

const processExistingClient = async (clientId: number): Promise<Client> => {
  const client = await Client.findOne(clientId)
  if (!client) {
    throw new ErrorHandler(404, 'Client with this ID does not exist')
  }
  return client
}

const processNewProducts = async (productsData: Object[]): Promise<Product[]> => {
  productsData.forEach((newProduct: Object) => {
    const errorsObj = ValidateRequiredFields(['name', 'price'], newProduct)

    if (Object.keys(errorsObj).length > 0) {
      throw new ErrorHandler(500, errorsObj, 'ValidationError')
    }
  })
  return Promise.all(productsData.map(async product => {
    const newProd = Object.assign(new Product(), product)
    await newProd.save()
    return newProd
  }))
}

const processExistingProducts = async (products: any[]): Promise<Product[]> => {
  let productsList: Product[] = []
  if (products.length > 0) {
    productsList = await Product.find({
      where: products.map(product => { return { id: product.id } })
    })

    if (productsList.length <= 0) {
      throw new ErrorHandler(404, 'Product(s) with this ID could not be found')
    }
  }
  return productsList
}

const processAllProducts = async (products: any[]): Promise<Product[] | void> => {
  if (!(products && products.length > 0)) return
  const existingProducts = products.filter(product => product.id)
  const newProducts = products.filter(product => !product.id)

  return Promise.all([...await processExistingProducts(existingProducts), ...await processNewProducts(newProducts)])
}

const totUpProducts = (products: any[]) => {
  let tot = 0
  products.forEach(product => {
    tot += parseFloat(product.price)
  })
  return tot
}

// Retrieving all invoices
router.get('/', async (req: Request, res: Response) => {
  const allInvoices = await Invoice.find({ relations: ['client', 'products'] })
  res.status(200).json(allInvoices)
})

// Retrieving invoice by ID
router.get('/:id', async (req: Request, res: Response) => {
  const { id } = req.params
  const invoice = await Invoice.findOne(id, { relations: ['client', 'products'] })
  res.status(200).json(invoice)
})

// Adding new invoice
router.post('/', async (req: Request, res: Response, next: NextFunction) => {
  const { date, client, products } = req.body

  try {
    // validating required fields
    const errorsObj: { [key: string]: any } = ValidateRequiredFields(['client', 'products'], req.body)

    if (Object.keys(errorsObj).length > 0) {
      throw new ErrorHandler(500, errorsObj, 'ValidationError')
    }

    if (products.length <= 0) {
      throw new ErrorHandler(500, 'Products array need to have at least one product', 'ValidationError')
    }

    // adding new or updating existing client
    const _client = client && client.id ? await processExistingClient(client.id) : await processNewClient(client)

    // adding new or updating existing product within the products array
    const _products = await processAllProducts(products)

    const invoice = new Invoice()
    Object.assign(invoice, {
      date: date || new Date(),
      ...(_client && { client: _client }),
      ...(_products && { products: _products }),
      ...(_products && { invoiceTotal: totUpProducts(_products) })
    })

    await invoice.save()
    return res.status(200).send('Invoice Successfully Added')

  } catch (e) {
    return next(e)
  }
})

// Updating existing invoice
router.put('/:id', async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params
  const { client, products } = req.body

  try {
    let invoiceToUpdate = await Invoice.findOne(id, { relations: ['client', 'products'] })

    if (products && products.length <= 0) {
      throw new ErrorHandler(500, 'Products array need to have at least one product')
    }

    if (invoiceToUpdate) {
      const filteredData = FilterRelevantFields(invoiceToUpdate, req.body)

      // adding new or updating existing client
      const _client = client && client.id ? await processExistingClient(client.id) : await processNewClient(client)

      // adding new or pdating existing product within the products array
      const _products = await processAllProducts(products)

      Object.assign(invoiceToUpdate, {
        ...filteredData,
        ...(_client && { client: _client }),
        ...(_products && { products: _products }),
        ...(_products && { invoiceTotal: totUpProducts(_products) })
      })

      await invoiceToUpdate.save()
      res.status(200).send('Invoice Data Successfully Updated')
    } else {
      throw new ErrorHandler(404, 'Invoice with this ID does not exist')
    }
  } catch (e) {
    next(e)
  }
})

// Deleting existing invoice
router.delete('/:id', async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params
  const invoiceToDelete = await Invoice.findOne(id)

  if (invoiceToDelete) {
    await invoiceToDelete.remove()
    res.status(200).send('Invoice Successfully Removed')
  } else {
    next(new ErrorHandler(404, 'Invoice with this ID does not exist'))
  }
})

export default router
