import express, { Request, Response, NextFunction } from 'express'
import { Product } from '../models/product'
import { ValidateRequiredFields, FilterRelevantFields } from '../utils'
import { ErrorHandler } from '../errorHandler'

const router = express.Router()

// Retrieving all products
router.get('/', async (req: Request, res: Response) => {
  const allProducts = await Product.find()
  res.status(200).json(allProducts)
})

// Retrieving product by ID
router.get('/:id', async (req: Request, res: Response) => {
  const { id } = req.params
  const product = await Product.findOne(id)
  res.status(200).json(product)
})

// Adding new product
router.post('/', async (req: Request, res: Response, next: NextFunction) => {
  const { name, price } = req.body

  try {
    // validating required fields
    const errorsObj: { [key: string]: any } = ValidateRequiredFields(['name', 'price'], req.body)

    if (Object.keys(errorsObj).length > 0) {
      throw new ErrorHandler(500, errorsObj, 'ValidationError')
    }

    const product = new Product()
    product.name = name
    product.price = price

    await product.save()

    res.status(200).send('Product Successfully Added')

  } catch (e) {
    next(e)
  }
})

// Updating existing product
router.put('/:id', async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params
  let productToUpdate = await Product.findOne(id)

  if (productToUpdate) {
    const filteredData = FilterRelevantFields(productToUpdate, req.body)

    Object.assign(productToUpdate, filteredData)
    await productToUpdate.save()
    res.status(200).send('Product Data Successfully Updated')
  } else {
    next(new ErrorHandler(404, 'Product with this ID does not exist'))
  }
})

// Deleting existing product
router.delete('/:id', async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params
  const productToDelete = await Product.findOne(id)

  if (productToDelete) {
    await productToDelete.remove()
    res.status(200).send('Product Successfully Removed')
  } else {
    next(new ErrorHandler(404, 'Product with this ID does not exist'))
  }
})

export default router
