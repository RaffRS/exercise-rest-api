import express, { Request, Response, NextFunction } from 'express'
import { Client } from '../models/client'
import { ValidateRequiredFields, FilterRelevantFields } from '../utils'
import { ErrorHandler } from '../errorHandler'

const router = express.Router()

// Retrieving all clients
router.get('/', async (req: Request, res: Response) => {
  const allClients = await Client.find()
  res.status(200).json(allClients)
})

// Retrieving client by ID
router.get('/:id', async (req: Request, res: Response) => {
  const { id } = req.params
  const client = await Client.findOne(id)
  res.status(200).json(client)
})

// Adding new client
router.post('/', async (req: Request, res: Response, next: NextFunction) => {
  const { name, streetAddress, city, phone, email } = req.body

  try {
    // validating required fields
    const errorsObj: { [key: string]: any } = ValidateRequiredFields(['name', 'streetAddress', 'city'], req.body)

    if (Object.keys(errorsObj).length > 0) {
      throw new ErrorHandler(500, errorsObj, 'ValidationError')
    }

    const client = new Client()
    client.name = name
    client.streetAddress = streetAddress
    client.city = city
    client.phone = phone
    client.email = email
    await client.save()

    res.status(200).send('Client Successfully Added')
  } catch (e) {
    next(e)
  }
})

// Updating existing client
router.put('/:id', async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params

  let clientToUpdate = await Client.findOne(id)

  if (clientToUpdate) {
    const filteredData = FilterRelevantFields(clientToUpdate, req.body)

    Object.assign(clientToUpdate, filteredData)
    await clientToUpdate.save()
    res.status(200).send('Client Data Successfully Updated')
  } else {
    next(new ErrorHandler(404, 'Client with this ID does not exist'))
  }

})

// Deleting existing client
router.delete('/:id', async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params
  const clientToDelete = await Client.findOne(id)

  if (clientToDelete) {
    await clientToDelete.remove()
    res.status(200).send('Client Successfully Removed')
  } else {
    next(new ErrorHandler(404, 'Client with this ID does not exist'))
  }
})

export default router
