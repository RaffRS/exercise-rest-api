import 'reflect-metadata'
import express, { Request, Response, NextFunction } from 'express'
import bodyParser from 'body-parser'
import { createConnection } from 'typeorm'
import invoiceRoutes from './routes/invoices'
import clientRoutes from './routes/clients'
import productRoutes from './routes/products'
import { handleError, ErrorHandler } from './errorHandler'

const port: string | number = process.env.PORT || 6060

createConnection().then(() => {
  const api = express()

  api.use(bodyParser.json())

  api.use('/invoices', invoiceRoutes)
  api.use('/clients', clientRoutes)
  api.use('/products', productRoutes)

  api.use((err: any, req: Request, res: Response, next: NextFunction) => {
    if (err instanceof ErrorHandler) {
      handleError(err, res)
    } else {
      console.error(err)
      res.status(500).send('Request could not be completed')
    }
  })

  api.listen(port, () => console.log(`Listening on port ${port}!`))
})
. catch(e => console.log(e))
