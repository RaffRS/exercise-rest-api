import { Entity, PrimaryGeneratedColumn, Column, ManyToMany, BaseEntity } from 'typeorm'
import { Invoice } from './invoice'

@Entity()
export class Product extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number

  @Column()
  name: string

  @Column({ type: 'decimal', precision: 13, scale: 2 })
  price: number

  @ManyToMany(type => Invoice, invoice => invoice.products)
  invoices: Invoice[]
}
