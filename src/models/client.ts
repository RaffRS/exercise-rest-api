import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, OneToMany } from 'typeorm'
import { Invoice } from './invoice'

@Entity()
export class Client extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number

  @Column({ default: null, nullable: true })
  name: string

  @Column()
  streetAddress: string

  @Column()
  city: string

  @Column({ default: null, nullable: true })
  phone: string

  @Column({ default: null, nullable: true })
  email: string

  @OneToMany(type => Invoice, invoice => invoice.client)
  invoices: Invoice[]
}
