import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, ManyToOne, ManyToMany, JoinColumn, JoinTable } from 'typeorm'
import { Client } from './client'
import { Product } from './product'

@Entity()
export class Invoice extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number

  @Column({ type: 'date' })
  date: Date

  @ManyToOne(type => Client, client => client)
  @JoinColumn()
  client: Client

  @ManyToMany(type => Product, product => product.invoices)
  @JoinTable()
  products: Product[]

  @Column({ type: 'decimal', precision: 13, scale: 2, default: 0 })
  invoiceTotal: number

}
