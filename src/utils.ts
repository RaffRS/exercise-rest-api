export const ValidateRequiredFields = (requiredFields: string[], dataObj: object): object => {
  let errorsObj: {
    [key: string]: any
  } = {}

  requiredFields
    .filter(field => !(field in dataObj))
    .forEach(field => {
      errorsObj[field] = `Field is required`
    })

  return errorsObj
}

export const FilterRelevantFields = (entity: object, dataObj: {[key: string]: any}): object => {
  const filteredData: {
    [key: string]: any
  } = {}

  Object.keys(entity).forEach(key => {
    if (key !== 'id' && key in dataObj) {
      filteredData[key] = dataObj[key]
    }
  })

  return filteredData
}
