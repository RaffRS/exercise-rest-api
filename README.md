### Invoice Rest API Task

To run the app, please run `npm install` and then `npm start`, the app should be available on http://localhost:6060


###Possible Routes

#### Clients 

`get`  /clients - Retrieves all clients

*Example response*

	[
		{
			"id": 1,
			"name": "SportsKits UK",
			"streetAddress": "Beechwood Avenue",
			"city": "Tamworth",
			"phone": "01544444444",
			"email": "shop@superstore.com"
		},
		{
			"id": 2,
			"name": "SportsKits UK",
			"streetAddress": "Beechwood Avenue",
			"city": "Tamworth",
			"phone": null,
			"email": "shop@superstore.com"
		}
	]
	
<br/>
`get` /clients:id - Retrieves specific client

| Parameters  |  |
| ------------- | ------------- |
| *id  | int  |

*Example response*

	{
		"id": 2,
		"name": "SportsKits UK",
		"streetAddress": "Beechwood Avenue",
		"city": "Tamworth",
		"phone": null,
		"email": "shop@superstore.com"
	}

<br>
`post` /clients - Creates new client

| Attributes  |  |
| ------------- | ------------- |
| *name  | string  |
| *streetAddress  | string  |
| *city  | string  |
| phone  | string  |
| email  | string  |

*Example request body*

	{
		"name": "SportsKits UK",
		"streetAddress": "Beechwood Avenue",
		"city": "Tamworth",
		"phone": "01543000000",
		"email": "shop@superstore.com"
	}

<br/>
`put` /clients:id - Updates existing client

| Parameters  |  |
| ------------- | ------------- |
| *id  | int  |

| Attributes  |  |
| ------------- | ------------- |
| name  | string  |
| streetAddress  | string  |
| city  | string  |
| phone  | string  |
| email  | string  |

*Example request body*

	{
		"city": "Stoke",
		"phone": "01543000000",
		"email": "shop@superstore.com"
	}

<br/>
`delete` /clients:id - Deletes existing client

| Parameters  |  |
| ------------- | ------------- |
| *id  | int  |


<br><br>
#### Products 

`get`  /products - Retrieves all products

*Example response*

	[
		{
			"id": 1,
			"name": "Consultancy",
			"price": 999.99
		},
		{
			"id": 2,
			"name": "Web Design",
			"price": 250.00
		}
	]

<br/>
`get` /products:id - Retrieves specific product

| Parameters  |  |
| ------------- | ------------- |
| *id  | int  |

*Example response*

	{
		"id": 2,
		"name": "Web Design",
		"price": 250.00
	}

<br>
`post` /products - Creates new product

| Attributes  |  |
| ------------- | ------------- |
| *name  | string  |
| *price  | int  |

*Example request body*

	{
		"name": "Printing Service",
		"price": 129.99
	}

<br/>
`put` /products:id - Updates existing client

| Attributes  |  |
| ------------- | ------------- |
| *name  | string  |
| *price  | int  |

*Example request body*

	{
		"name": "Printing Service",
		"price": 119.99
	}

<br/>
`delete` /products:id - Deletes existing product

| Parameters  |  |
| ------------- | ------------- |
| *id  | int  |


<br><br>
#### Invoices 

`get`  /invoices - Retrieves all invoices

*Example response*

	[
		{
			"id": 1,
			"date": "2019/11/28",
			"invoiceTotal": 1198.99,
			"client": {
				"id": 1,
				"name": "SportsKits UK",
				"streetAddress": "Beechwood Avenue",
				"city": "Tamworth",
				"phone": "01544444444",
				"email": "shop@superstore.com"
			},
			"products": [
				{
					"id": 1,
					"name": "Web design",
					"price": 999.99
				},
				{
					"id": 2,
					"name": "Consultancy",
					"price": 199
				}
			]
		},
		{
			"id": 2,
			"date": "2019/11/28",
			"invoiceTotal": 478,
			"client": {
				"id": 2,
				"name": "SportsKits UK",
				"streetAddress": "Beechwood Avenue",
				"city": "Tamworth",
				"phone": null,
				"email": "shop@superstore.com"
			},
			"products": [
				{
					"id": 2,
					"name": "Consultancy",
					"price": 199
				},
				{
					"id": 3,
					"name": "Business Cards Design",
					"price": 180
				},
				{
					"id": 4,
					"name": "Printing Service",
					"price": 99
				}
			]
		}
	]

<br/>
`get` /invoices:id - Retrieves specific invoice

| Parameters  |  |
| ------------- | ------------- |
| *id  | int  |

*Example response*

	{
		"id": 2,
		"date": "28/12/2019",
		"invoiceTotal": 999.99,
		"client": {
			"id": 2,
			"name": "SportsKits UK",
			"streetAddress": "Beechwood Avenue",
			"city": "Tamworth",
			"phone": null,
			"email": "shop@superstore.com"
		},
		"products": [
			{
				"id": 1,
				"name": "Web design",
				"price": 999.99
			}
		]
	}

<br>
`post` /invoices - Creates new invoice

| Attributes  |  |  |
| ------------- | ------------- |
| *client  | object  | Object defining client id
| *products  | array  | Array of product objects - **if ID property exists in the object, no other properties will be respected and the provided ID will be associated with the invoice.  Objects containing properties  other than ID, will result in new product creation attempt**
| date  | date  | If not provided, will default to current date

*Example request body*

	{ 
	   "client":{ 
		  "id": 3
	   },
	   "products":[ 
		  { 
			 "id": 2
		  },
		  { 
			 "name": "Consultancy",
			 "price": 199.99
		  }
	   ],
	    "date": "12/12/2019
	}

<br/>
`put` /invoices:id - Updates existing invoice

| Attributes  |  |  |
| ------------- | ------------- |
| *client  | object  | Object defining client id
| *products  | array  | Array of product objects (completely replaces existing products array) - **if ID property exists in the object, no other properties will be respected and the provided ID will be associated with the invoice.  Objects containing properties  other than ID, will result in new product creation attempt** 
| date  | date  | If not provided, will default to current date

*Example request body*

	{ 
	   "client":{ 
		  "id": 2
	   },
	   "products":[ 
		  { 
			 "id": 1
		  },
		  { 
			 "name": "Web design",
			 "price": 129.00
		  }
	   ],
	   "date": "12/12/2019
	}

<br/>
`delete` /invoices:id - Deletes existing invoice

| Parameters  |  |
| ------------- | ------------- |
| *id  | int  |

